# Powerboardv3
Powerboardv3 PCB design files (KiCad)
## Barrel
  * Schematic: [pbv3_schematic.pdf](https://gitlab.cern.ch/theim/powerboard_v3/raw/master/barrel/pbv3_schematic.pdf)
  * Gerbers: [gerbers.zip](https://gitlab.cern.ch/theim/powerboard_v3/raw/master/barrel/gerbers/gerbers.zip)

![3D render](https://gitlab.cern.ch/theim/powerboard_v3/raw/master/pbv3.png "3D render")

