EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 5E8785E7
P 1100 950
F 0 "R1" H 1170 996 50  0000 L CNN
F 1 "R" H 1170 905 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1030 950 50  0001 C CNN
F 3 "~" H 1100 950 50  0001 C CNN
	1    1100 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5E878651
P 1300 950
F 0 "R2" H 1370 996 50  0000 L CNN
F 1 "R" H 1370 905 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1230 950 50  0001 C CNN
F 3 "~" H 1300 950 50  0001 C CNN
	1    1300 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5E878696
P 1500 950
F 0 "R3" H 1570 996 50  0000 L CNN
F 1 "R" H 1570 905 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1430 950 50  0001 C CNN
F 3 "~" H 1500 950 50  0001 C CNN
	1    1500 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5E8786D5
P 1700 950
F 0 "R4" H 1770 996 50  0000 L CNN
F 1 "R" H 1770 905 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1630 950 50  0001 C CNN
F 3 "~" H 1700 950 50  0001 C CNN
	1    1700 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5E878714
P 1900 950
F 0 "R5" H 1970 996 50  0000 L CNN
F 1 "R" H 1970 905 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1830 950 50  0001 C CNN
F 3 "~" H 1900 950 50  0001 C CNN
	1    1900 950 
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 5E878756
P 1100 1450
F 0 "R6" H 1170 1496 50  0000 L CNN
F 1 "R" H 1170 1405 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1030 1450 50  0001 C CNN
F 3 "~" H 1100 1450 50  0001 C CNN
	1    1100 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5E8787A2
P 1300 1450
F 0 "R7" H 1370 1496 50  0000 L CNN
F 1 "R" H 1370 1405 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1230 1450 50  0001 C CNN
F 3 "~" H 1300 1450 50  0001 C CNN
	1    1300 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5E8787E7
P 1500 1450
F 0 "R8" H 1570 1496 50  0000 L CNN
F 1 "R" H 1570 1405 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1430 1450 50  0001 C CNN
F 3 "~" H 1500 1450 50  0001 C CNN
	1    1500 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5E878834
P 1700 1450
F 0 "R9" H 1770 1496 50  0000 L CNN
F 1 "R" H 1770 1405 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1630 1450 50  0001 C CNN
F 3 "~" H 1700 1450 50  0001 C CNN
	1    1700 1450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R10
U 1 1 5E878881
P 1900 1450
F 0 "R10" H 1970 1496 50  0000 L CNN
F 1 "R" H 1970 1405 50  0000 L CNN
F 2 "Resistors_SMD:R_0201_NoSilk" V 1830 1450 50  0001 C CNN
F 3 "~" H 1900 1450 50  0001 C CNN
	1    1900 1450
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5E878F68
P 2900 2550
F 0 "TP1" V 2854 2738 50  0000 L CNN
F 1 "TestPoint" V 2945 2738 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 3100 2550 50  0001 C CNN
F 3 "~" H 3100 2550 50  0001 C CNN
	1    2900 2550
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 5E87900E
P 4000 2550
F 0 "TP2" V 3954 2738 50  0000 L CNN
F 1 "TestPoint" V 4045 2738 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4200 2550 50  0001 C CNN
F 3 "~" H 4200 2550 50  0001 C CNN
	1    4000 2550
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5E879087
P 2900 2650
F 0 "TP3" V 2854 2838 50  0000 L CNN
F 1 "TestPoint" V 2945 2838 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 3100 2650 50  0001 C CNN
F 3 "~" H 3100 2650 50  0001 C CNN
	1    2900 2650
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP4
U 1 1 5E87912E
P 4000 2650
F 0 "TP4" V 3954 2838 50  0000 L CNN
F 1 "TestPoint" V 4045 2838 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4200 2650 50  0001 C CNN
F 3 "~" H 4200 2650 50  0001 C CNN
	1    4000 2650
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP5
U 1 1 5E8791AF
P 2900 2750
F 0 "TP5" V 2854 2938 50  0000 L CNN
F 1 "TestPoint" V 2945 2938 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 3100 2750 50  0001 C CNN
F 3 "~" H 3100 2750 50  0001 C CNN
	1    2900 2750
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP6
U 1 1 5E87923E
P 4000 2750
F 0 "TP6" V 3954 2938 50  0000 L CNN
F 1 "TestPoint" V 4045 2938 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4200 2750 50  0001 C CNN
F 3 "~" H 4200 2750 50  0001 C CNN
	1    4000 2750
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP7
U 1 1 5E8792CC
P 2900 2850
F 0 "TP7" V 2854 3038 50  0000 L CNN
F 1 "TestPoint" V 2945 3038 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 3100 2850 50  0001 C CNN
F 3 "~" H 3100 2850 50  0001 C CNN
	1    2900 2850
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP8
U 1 1 5E879366
P 4000 2850
F 0 "TP8" V 3954 3038 50  0000 L CNN
F 1 "TestPoint" V 4045 3038 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4200 2850 50  0001 C CNN
F 3 "~" H 4200 2850 50  0001 C CNN
	1    4000 2850
	0    1    1    0   
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J1
U 1 1 5E94BFC2
P 3400 2750
F 0 "J1" H 3450 3167 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 3450 3076 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x06_Pitch1.27mm_SMD" H 3400 2750 50  0001 C CNN
F 3 "~" H 3400 2750 50  0001 C CNN
	1    3400 2750
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP9
U 1 1 5E94CC8B
P 2900 2950
F 0 "TP9" V 2854 3138 50  0000 L CNN
F 1 "TestPoint" V 2945 3138 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 3100 2950 50  0001 C CNN
F 3 "~" H 3100 2950 50  0001 C CNN
	1    2900 2950
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP11
U 1 1 5E94CCC1
P 2900 3050
F 0 "TP11" V 2854 3238 50  0000 L CNN
F 1 "TestPoint" V 2945 3238 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 3100 3050 50  0001 C CNN
F 3 "~" H 3100 3050 50  0001 C CNN
	1    2900 3050
	0    -1   -1   0   
$EndComp
$Comp
L Connector:TestPoint TP10
U 1 1 5E94CCF9
P 4000 2950
F 0 "TP10" V 3954 3138 50  0000 L CNN
F 1 "TestPoint" V 4045 3138 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4200 2950 50  0001 C CNN
F 3 "~" H 4200 2950 50  0001 C CNN
	1    4000 2950
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP12
U 1 1 5E94CD7E
P 4000 3050
F 0 "TP12" V 3954 3238 50  0000 L CNN
F 1 "TestPoint" V 4045 3238 50  0000 L CNN
F 2 "Measurement_Points:Measurement_Point_Round-SMD-Pad_Small" H 4200 3050 50  0001 C CNN
F 3 "~" H 4200 3050 50  0001 C CNN
	1    4000 3050
	0    1    1    0   
$EndComp
Wire Wire Line
	3700 2750 3850 2750
Wire Wire Line
	3700 2850 3850 2850
Wire Wire Line
	3850 2850 3850 2750
Connection ~ 3850 2850
Wire Wire Line
	3850 2850 4000 2850
Connection ~ 3850 2750
Wire Wire Line
	3850 2750 4000 2750
Wire Wire Line
	2900 2650 3050 2650
Wire Wire Line
	3200 2550 3050 2550
Wire Wire Line
	3050 2650 3050 2550
Connection ~ 3050 2650
Wire Wire Line
	3050 2650 3200 2650
Connection ~ 3050 2550
Wire Wire Line
	3050 2550 2900 2550
Wire Wire Line
	2900 3050 3200 3050
Wire Wire Line
	2900 2950 3200 2950
Wire Wire Line
	3700 3050 3850 3050
Wire Wire Line
	3700 2950 3850 2950
Wire Wire Line
	3700 2550 4000 2550
Wire Wire Line
	3700 2650 4000 2650
Wire Wire Line
	2900 2850 3200 2850
Wire Wire Line
	2900 2750 3200 2750
Wire Wire Line
	3850 2950 3850 3050
Connection ~ 3850 2950
Wire Wire Line
	3850 2950 4000 2950
Connection ~ 3850 3050
Wire Wire Line
	3850 3050 4000 3050
Wire Wire Line
	3850 2850 3850 2950
$EndSCHEMATC
