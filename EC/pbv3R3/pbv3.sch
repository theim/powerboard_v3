EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 11693 16535 portrait
encoding utf-8
Sheet 1 7
Title "Powerboard v3"
Date "2017-10-23"
Rev "A"
Comp "LBNL"
Comment1 "Timon Heim"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 7150 900  0    260  ~ 0
AMAC
$Sheet
S 2850 5550 1050 1000
U 59F07AAA
F0 "HV" 60
F1 "hv.sch" 60
F2 "HVOSC" I L 2850 5700 60 
F3 "GND" I L 2850 5850 60 
F4 "HV_IN" I L 2850 6150 60 
F5 "HV_IN_RTN" I L 2850 6300 60 
F6 "HV_OUT_RTN" I R 3900 6050 60 
F7 "HV_OUT" I R 3900 5900 60 
$EndSheet
Text Notes 5600 900  0    260  ~ 0
POWER
Text Notes 6300 4900 0    260  ~ 0
HV
$Comp
L pbv3:BondPad J10
U 1 1 59F0A45B
P 4850 5900
F 0 "J10" H 4850 6000 60  0000 C CNN
F 1 "HV_OUT" H 4850 5800 60  0000 C CNN
F 2 "pbv3:pad" H 4850 5900 60  0001 C CNN
F 3 "" H 4850 5900 60  0001 C CNN
	1    4850 5900
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 59F0A65E
P 2600 5950
F 0 "#PWR0103" H 2600 5700 50  0001 C CNN
F 1 "GND" H 2600 5800 50  0000 C CNN
F 2 "" H 2600 5950 50  0000 C CNN
F 3 "" H 2600 5950 50  0000 C CNN
	1    2600 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 59F2094A
P 7700 2100
F 0 "#PWR0106" H 7700 1850 50  0001 C CNN
F 1 "GND" H 7700 1950 50  0000 C CNN
F 2 "" H 7700 2100 50  0000 C CNN
F 3 "" H 7700 2100 50  0000 C CNN
	1    7700 2100
	1    0    0    -1  
$EndComp
Text Label 7950 2400 0    60   ~ 0
FVOUT
Text Label 7950 2650 0    60   ~ 0
FVIN
Text Label 7950 2300 0    60   ~ 0
VOUT
Text Label 7950 2550 0    60   ~ 0
VIN
Text Label 9800 1800 0    60   ~ 0
DCDCEN
Text Label 9800 1900 0    60   ~ 0
PGOOD
Text Label 9800 2000 0    60   ~ 0
PTAT
Text Label 9800 2100 0    60   ~ 0
NTCPBn
Text Label 9800 2200 0    60   ~ 0
NTCPBp
$Comp
L pbv3:BondPad J16
U 1 1 59F2B21A
P 8000 3100
F 0 "J16" H 8000 3200 60  0000 C CNN
F 1 "HRSTBx" H 7700 3100 60  0000 C CNN
F 2 "pbv3:pad" H 8000 3100 60  0001 C CNN
F 3 "" H 8000 3100 60  0001 C CNN
	1    8000 3100
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J17
U 1 1 59F2B4B6
P 8000 3300
F 0 "J17" H 8000 3400 60  0000 C CNN
F 1 "LDx0EN" H 7700 3300 60  0000 C CNN
F 2 "pbv3:pad" H 8000 3300 60  0001 C CNN
F 3 "" H 8000 3300 60  0001 C CNN
	1    8000 3300
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J18
U 1 1 59F2B758
P 8000 3500
F 0 "J18" H 8000 3600 60  0000 C CNN
F 1 "HREFx" H 7700 3500 60  0000 C CNN
F 2 "pbv3:pad" H 8000 3500 60  0001 C CNN
F 3 "" H 8000 3500 60  0001 C CNN
	1    8000 3500
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J19
U 1 1 59F2CFAB
P 8000 3700
F 0 "J19" H 8000 3800 60  0000 C CNN
F 1 "NTCxP" H 7700 3700 60  0000 C CNN
F 2 "pbv3:pad" H 8000 3700 60  0001 C CNN
F 3 "" H 8000 3700 60  0001 C CNN
	1    8000 3700
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J20
U 1 1 59F2D40A
P 8000 3900
F 0 "J20" H 8000 4000 60  0000 C CNN
F 1 "NTCxN" H 7700 3900 60  0000 C CNN
F 2 "pbv3:pad" H 8000 3900 60  0001 C CNN
F 3 "" H 8000 3900 60  0001 C CNN
	1    8000 3900
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J21
U 1 1 59F2D6A1
P 8000 4100
F 0 "J21" H 8000 4200 60  0000 C CNN
F 1 "LDx1EN" H 7700 4100 60  0000 C CNN
F 2 "pbv3:pad" H 8000 4100 60  0001 C CNN
F 3 "" H 8000 4100 60  0001 C CNN
	1    8000 4100
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J15
U 1 1 59F365AA
P 8000 2900
F 0 "J15" H 8000 3000 60  0000 C CNN
F 1 "SSSHx" H 7700 2900 60  0000 C CNN
F 2 "pbv3:pad" H 8000 2900 60  0001 C CNN
F 3 "" H 8000 2900 60  0001 C CNN
	1    8000 2900
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J24
U 1 1 59F36857
P 8000 4700
F 0 "J24" H 8000 4800 60  0000 C CNN
F 1 "CALx" H 7700 4700 60  0000 C CNN
F 2 "pbv3:pad" H 8000 4700 60  0001 C CNN
F 3 "" H 8000 4700 60  0001 C CNN
	1    8000 4700
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J23
U 1 1 59F36B07
P 8000 4500
F 0 "J23" H 8000 4600 60  0000 C CNN
F 1 "SHUNTx" H 7700 4500 60  0000 C CNN
F 2 "pbv3:pad" H 8000 4500 60  0001 C CNN
F 3 "" H 8000 4500 60  0001 C CNN
	1    8000 4500
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J22
U 1 1 59F36DB7
P 8000 4300
F 0 "J22" H 8000 4400 60  0000 C CNN
F 1 "LDx2EN" H 7700 4300 60  0000 C CNN
F 2 "pbv3:pad" H 8000 4300 60  0001 C CNN
F 3 "" H 8000 4300 60  0001 C CNN
	1    8000 4300
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J30
U 1 1 59F3BEC5
P 9800 3100
F 0 "J30" H 9800 3200 60  0000 C CNN
F 1 "HRSTBy" H 9500 3100 60  0000 C CNN
F 2 "pbv3:pad" H 9800 3100 60  0001 C CNN
F 3 "" H 9800 3100 60  0001 C CNN
	1    9800 3100
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J31
U 1 1 59F3BECB
P 9800 3300
F 0 "J31" H 9800 3400 60  0000 C CNN
F 1 "LDy0EN" H 9500 3300 60  0000 C CNN
F 2 "pbv3:pad" H 9800 3300 60  0001 C CNN
F 3 "" H 9800 3300 60  0001 C CNN
	1    9800 3300
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J32
U 1 1 59F3BED1
P 9800 3500
F 0 "J32" H 9800 3600 60  0000 C CNN
F 1 "HREFy" H 9500 3500 60  0000 C CNN
F 2 "pbv3:pad" H 9800 3500 60  0001 C CNN
F 3 "" H 9800 3500 60  0001 C CNN
	1    9800 3500
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J33
U 1 1 59F3BED7
P 9800 3700
F 0 "J33" H 9800 3800 60  0000 C CNN
F 1 "NTCyN" H 9500 3700 60  0000 C CNN
F 2 "pbv3:pad" H 9800 3700 60  0001 C CNN
F 3 "" H 9800 3700 60  0001 C CNN
	1    9800 3700
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J34
U 1 1 59F3BEDD
P 9800 3900
F 0 "J34" H 9800 4000 60  0000 C CNN
F 1 "NTCyP" H 9500 3900 60  0000 C CNN
F 2 "pbv3:pad" H 9800 3900 60  0001 C CNN
F 3 "" H 9800 3900 60  0001 C CNN
	1    9800 3900
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J35
U 1 1 59F3BEE3
P 9800 4100
F 0 "J35" H 9800 4200 60  0000 C CNN
F 1 "LDy1EN" H 9500 4100 60  0000 C CNN
F 2 "pbv3:pad" H 9800 4100 60  0001 C CNN
F 3 "" H 9800 4100 60  0001 C CNN
	1    9800 4100
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J29
U 1 1 59F3BEE9
P 9800 2900
F 0 "J29" H 9800 3000 60  0000 C CNN
F 1 "SSSHy" H 9500 2900 60  0000 C CNN
F 2 "pbv3:pad" H 9800 2900 60  0001 C CNN
F 3 "" H 9800 2900 60  0001 C CNN
	1    9800 2900
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J38
U 1 1 59F3BEEF
P 9800 4700
F 0 "J38" H 9800 4800 60  0000 C CNN
F 1 "CAly" H 9500 4700 60  0000 C CNN
F 2 "pbv3:pad" H 9800 4700 60  0001 C CNN
F 3 "" H 9800 4700 60  0001 C CNN
	1    9800 4700
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J37
U 1 1 59F3BEF5
P 9800 4500
F 0 "J37" H 9800 4600 60  0000 C CNN
F 1 "SHUNTy" H 9500 4500 60  0000 C CNN
F 2 "pbv3:pad" H 9800 4500 60  0001 C CNN
F 3 "" H 9800 4500 60  0001 C CNN
	1    9800 4500
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J36
U 1 1 59F3BEFB
P 9800 4300
F 0 "J36" H 9800 4400 60  0000 C CNN
F 1 "LDy2EN" H 9500 4300 60  0000 C CNN
F 2 "pbv3:pad" H 9800 4300 60  0001 C CNN
F 3 "" H 9800 4300 60  0001 C CNN
	1    9800 4300
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J25
U 1 1 59F402B6
P 8000 5000
F 0 "J25" H 8000 5100 60  0000 C CNN
F 1 "HVretX" H 7700 5000 60  0000 C CNN
F 2 "pbv3:pad" H 8000 5000 60  0001 C CNN
F 3 "" H 8000 5000 60  0001 C CNN
	1    8000 5000
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J40
U 1 1 59F4095C
P 10150 5400
F 0 "J40" H 10150 5500 60  0000 C CNN
F 1 "CMDoutP" H 9850 5400 60  0000 C CNN
F 2 "pbv3:pad" H 10150 5400 60  0001 C CNN
F 3 "" H 10150 5400 60  0001 C CNN
	1    10150 5400
	-1   0    0    1   
$EndComp
$Comp
L pbv3:BondPad J41
U 1 1 59F40C45
P 10150 5600
F 0 "J41" H 10150 5700 60  0000 C CNN
F 1 "CMDoutN" H 9850 5600 60  0000 C CNN
F 2 "pbv3:pad" H 10150 5600 60  0001 C CNN
F 3 "" H 10150 5600 60  0001 C CNN
	1    10150 5600
	-1   0    0    1   
$EndComp
$Comp
L pbv3:BondPad J26
U 1 1 59F41436
P 7600 5400
F 0 "J26" H 7600 5500 60  0000 C CNN
F 1 "CMDinP" H 7300 5400 60  0000 C CNN
F 2 "pbv3:pad" H 7600 5400 60  0001 C CNN
F 3 "" H 7600 5400 60  0001 C CNN
	1    7600 5400
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J27
U 1 1 59F41709
P 7600 5600
F 0 "J27" H 7600 5700 60  0000 C CNN
F 1 "CMDinN" H 7300 5600 60  0000 C CNN
F 2 "pbv3:pad" H 7600 5600 60  0001 C CNN
F 3 "" H 7600 5600 60  0001 C CNN
	1    7600 5600
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J28
U 1 1 59F425B4
P 7600 5750
F 0 "J28" H 7600 5850 60  0000 C CNN
F 1 "CAL" H 7300 5750 60  0000 C CNN
F 2 "pbv3:pad" H 7600 5750 60  0001 C CNN
F 3 "" H 7600 5750 60  0001 C CNN
	1    7600 5750
	1    0    0    -1  
$EndComp
Text Notes 7550 2650 0    60   ~ 0
50mOhm
Text Notes 7550 2400 0    60   ~ 0
10mOhm
Text Label 7950 5900 0    60   ~ 0
OFin
Text Label 9800 2400 0    60   ~ 0
GPO1
Text Label 9800 2300 0    60   ~ 0
GPO2
$Comp
L power:GND #PWR0102
U 1 1 59EF94B5
P 2400 6400
F 0 "#PWR0102" H 2400 6150 50  0001 C CNN
F 1 "GND" H 2400 6250 50  0000 C CNN
F 2 "" H 2400 6400 50  0000 C CNN
F 3 "" H 2400 6400 50  0000 C CNN
	1    2400 6400
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J4
U 1 1 59F0A7A4
P 2000 6050
F 0 "J4" H 2000 6150 60  0000 C CNN
F 1 "HV_IN" H 2000 5950 60  0000 C CNN
F 2 "pbv3:pad" H 2000 6050 60  0001 C CNN
F 3 "" H 2000 6050 60  0001 C CNN
	1    2000 6050
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 59F3F88D
P 4250 6150
F 0 "#PWR0104" H 4250 5900 50  0001 C CNN
F 1 "GND" H 4250 6000 50  0000 C CNN
F 2 "" H 4250 6150 50  0000 C CNN
F 3 "" H 4250 6150 50  0000 C CNN
	1    4250 6150
	1    0    0    -1  
$EndComp
Wire Notes Line
	550  550  6950 550 
Wire Notes Line
	6950 4450 550  4450
Wire Notes Line
	550  4450 550  550 
Wire Wire Line
	4250 6150 4250 6050
Wire Notes Line
	550  4550 6850 4550
Wire Notes Line
	6850 4550 6850 7650
Wire Notes Line
	6850 7650 550  7650
Wire Notes Line
	550  7650 550  4550
Wire Notes Line
	7050 550  11050 550 
Wire Notes Line
	11050 550  11050 6350
Wire Notes Line
	11050 6350 7050 6350
Wire Notes Line
	7050 6350 7050 550 
Wire Notes Line
	6950 550  6950 4450
Wire Wire Line
	4250 6050 3900 6050
Wire Wire Line
	3900 5900 4600 5900
Wire Wire Line
	2850 5850 2600 5850
Wire Wire Line
	2600 5850 2600 5950
Wire Wire Line
	2850 6150 2400 6150
Wire Wire Line
	2850 6300 2400 6300
Wire Wire Line
	7700 2100 8400 2100
Wire Wire Line
	7950 2400 8400 2400
Wire Wire Line
	7950 2650 8400 2650
Wire Wire Line
	7950 2300 8400 2300
Wire Wire Line
	7950 2550 8400 2550
Wire Wire Line
	9400 1800 9800 1800
Wire Wire Line
	9400 1900 9800 1900
Wire Wire Line
	9400 2000 9800 2000
Wire Wire Line
	9400 2100 9800 2100
Wire Wire Line
	9400 2200 9800 2200
Wire Wire Line
	7350 5300 2550 5300
Wire Wire Line
	2550 5300 2550 5700
Wire Wire Line
	2550 5700 2850 5700
Wire Wire Line
	7350 5300 7350 5200
Wire Wire Line
	7350 5200 8400 5200
Wire Wire Line
	8250 2900 8400 2900
Wire Wire Line
	8250 3100 8400 3100
Wire Wire Line
	8250 3300 8400 3300
Wire Wire Line
	8250 3500 8400 3500
Wire Wire Line
	8250 3700 8400 3700
Wire Wire Line
	8250 3900 8400 3900
Wire Wire Line
	8250 4100 8400 4100
Wire Wire Line
	9400 2900 9550 2900
Wire Wire Line
	9400 3100 9550 3100
Wire Wire Line
	9400 3300 9550 3300
Wire Wire Line
	9400 3500 9550 3500
Wire Wire Line
	9400 3700 9550 3700
Wire Wire Line
	9400 3900 9550 3900
Wire Wire Line
	9400 4100 9550 4100
Wire Wire Line
	9400 4300 9550 4300
Wire Wire Line
	9400 4500 9550 4500
Wire Wire Line
	9400 4700 9550 4700
Wire Wire Line
	9400 5450 9500 5450
Wire Wire Line
	9500 5450 9500 5400
Wire Wire Line
	9400 5550 9500 5550
Wire Wire Line
	9500 5550 9500 5600
Wire Wire Line
	8400 5450 8300 5450
Wire Wire Line
	8300 5450 8300 5400
Wire Wire Line
	8400 5550 8300 5550
Wire Wire Line
	8300 5550 8300 5600
Wire Wire Line
	8250 4300 8400 4300
Wire Wire Line
	8250 4500 8400 4500
Wire Wire Line
	8250 4700 8400 4700
Wire Wire Line
	8250 5000 8400 5000
Wire Wire Line
	9400 2300 9800 2300
Wire Wire Line
	9400 2400 9800 2400
Wire Wire Line
	2400 6300 2400 6400
Wire Wire Line
	2400 6150 2400 6050
Wire Wire Line
	2400 6050 2250 6050
Wire Wire Line
	8150 1900 8150 1450
Wire Wire Line
	8150 1450 10400 1450
Wire Wire Line
	10400 1450 10400 5850
Wire Wire Line
	10400 5850 9400 5850
Text Label 8200 1450 0    60   ~ 0
VCC1V4
Wire Wire Line
	8150 1900 8400 1900
$Sheet
S 8400 1700 1000 4450
U 59F0798F
F0 "AMAC" 60
F1 "amac.sch" 60
F2 "VDCDC" I L 8400 1800 60 
F3 "VLREG" I L 8400 1900 60 
F4 "GND" I L 8400 2100 60 
F5 "CUR1Vp" I L 8400 2300 60 
F6 "CUR1Vn" I L 8400 2400 60 
F7 "DCDCEN" I R 9400 1800 60 
F8 "NTCPBn" I R 9400 2100 60 
F9 "NTCPBp" I R 9400 2200 60 
F10 "CUR10Vn" I L 8400 2650 60 
F11 "CUR10Vp" I L 8400 2550 60 
F12 "PGOOD" I R 9400 1900 60 
F13 "PTAT" I R 9400 2000 60 
F14 "SSSHy" I R 9400 2900 60 
F15 "HRSTBy" I R 9400 3100 60 
F16 "LDy0EN" I R 9400 3300 60 
F17 "HREFy" I R 9400 3500 60 
F18 "NTCyN" I R 9400 3700 60 
F19 "NTCyP" I R 9400 3900 60 
F20 "LDy1EN" I R 9400 4100 60 
F21 "LDy2EN" I R 9400 4300 60 
F22 "SHUNTy" I R 9400 4500 60 
F23 "CALy" I R 9400 4700 60 
F24 "OFout" I R 9400 6000 60 
F25 "GPO2" I R 9400 2300 60 
F26 "HVret1" I L 8400 5000 60 
F27 "CMDin_P" I L 8400 5450 60 
F28 "CMDin_N" I L 8400 5550 60 
F29 "CMDout_P" O R 9400 5450 60 
F30 "CMDout_N" I R 9400 5550 60 
F31 "HVOSC0" I L 8400 5200 60 
F32 "VDDHI" I L 8400 2000 60 
F33 "LAM" I R 9400 5850 60 
F34 "HVret2" I R 9400 5000 60 
F35 "LDx0EN" I L 8400 3300 60 
F36 "HRSTBx" I L 8400 3100 60 
F37 "SSSHx" I L 8400 2900 60 
F38 "HREFx" I L 8400 3500 60 
F39 "NTCxP" I L 8400 3700 60 
F40 "NTCxN" I L 8400 3900 60 
F41 "LDx1EN" I L 8400 4100 60 
F42 "LDx2EN" I L 8400 4300 60 
F43 "SHUNTx" I L 8400 4500 60 
F44 "CALx" I L 8400 4700 60 
F45 "OFin" I L 8400 5900 60 
F46 "CAL" I L 8400 5750 60 
F47 "GPO1" I R 9400 2400 60 
F48 "CMDin*_P" I L 8400 6000 60 
F49 "CMDin*_N" I L 8400 6100 60 
$EndSheet
Connection ~ 8150 1900
Wire Wire Line
	5500 2000 8400 2000
Wire Wire Line
	5200 1900 8150 1900
Wire Wire Line
	1900 2200 2650 2200
Wire Wire Line
	4200 1650 4200 1800
Wire Wire Line
	1950 1900 1950 1950
Connection ~ 5500 2000
Wire Wire Line
	5500 2100 5500 2000
Connection ~ 5200 1900
Wire Wire Line
	5200 2100 5200 1900
Wire Wire Line
	3850 2150 4400 2150
Wire Wire Line
	3850 2300 4400 2300
Connection ~ 1900 2200
Wire Wire Line
	1900 2200 1900 2500
Wire Wire Line
	2650 2900 2050 2900
Wire Wire Line
	2650 2750 2050 2750
Wire Wire Line
	2650 2350 2050 2350
Wire Wire Line
	3850 2900 4400 2900
Wire Wire Line
	3850 2750 4400 2750
Wire Wire Line
	3850 2600 4400 2600
Wire Wire Line
	3850 2450 4400 2450
Wire Wire Line
	4050 2000 5500 2000
Wire Wire Line
	4050 1950 4050 2000
Wire Wire Line
	3850 1950 4050 1950
Wire Wire Line
	4100 1900 5200 1900
Wire Wire Line
	4100 1800 4100 1900
Wire Wire Line
	3850 1800 4100 1800
Connection ~ 4200 1650
Wire Wire Line
	4200 1800 8400 1800
Wire Wire Line
	1800 2200 1900 2200
Connection ~ 1950 1900
Wire Wire Line
	1950 1650 2650 1650
Wire Wire Line
	1950 1550 1950 1650
Wire Wire Line
	1800 1550 1950 1550
Wire Wire Line
	1950 1800 2650 1800
Wire Wire Line
	1950 1800 1950 1900
Wire Wire Line
	1800 1900 1950 1900
Wire Wire Line
	5550 1500 5400 1500
Wire Wire Line
	4200 1500 4850 1500
Wire Wire Line
	4200 1500 4200 1650
Wire Wire Line
	3850 1650 4200 1650
Wire Wire Line
	5550 1350 5550 1500
Wire Wire Line
	4850 1350 4850 1500
$Comp
L pbv3:BondPad J6
U 1 1 5AABFC0C
P 5500 2350
F 0 "J6" H 5500 2450 60  0000 C CNN
F 1 "T3V3" H 5500 2250 60  0000 C CNN
F 2 "pbv3:pad" H 5500 2350 60  0001 C CNN
F 3 "" H 5500 2350 60  0001 C CNN
	1    5500 2350
	0    -1   -1   0   
$EndComp
$Comp
L pbv3:BondPad J5
U 1 1 5AABF9E9
P 5200 2350
F 0 "J5" H 5200 2450 60  0000 C CNN
F 1 "T1V4" H 5200 2250 60  0000 C CNN
F 2 "pbv3:pad" H 5200 2350 60  0001 C CNN
F 3 "" H 5200 2350 60  0001 C CNN
	1    5200 2350
	0    -1   -1   0   
$EndComp
Text Label 4400 2150 0    60   ~ 0
GPO1
Text Label 4400 2300 0    60   ~ 0
GPO2
Text Label 1900 2500 1    60   ~ 0
OFin
Text Notes 1850 1350 0    60   ~ 0
Fuse?
Text Label 2050 2900 0    60   ~ 0
NTCPBn
Text Label 2050 2750 0    60   ~ 0
NTCPBp
Text Label 2050 2350 0    60   ~ 0
DCDCEN
Text Label 4400 2900 0    60   ~ 0
PTAT
Text Label 4400 2750 0    60   ~ 0
PGOOD
Text Label 4400 2600 0    60   ~ 0
FVIN
Text Label 4400 2450 0    60   ~ 0
FVOUT
Text Label 2200 1650 0    60   ~ 0
VIN
Text Label 4300 1500 0    60   ~ 0
VOUT
$Comp
L pbv3:BondPad J3
U 1 1 59F06B61
P 1550 2200
F 0 "J3" H 1550 2300 60  0000 C CNN
F 1 "OFin" H 1550 2100 60  0000 C CNN
F 2 "pbv3:pad" H 1550 2200 60  0001 C CNN
F 3 "" H 1550 2200 60  0001 C CNN
	1    1550 2200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 59F064E3
P 1950 1950
F 0 "#PWR0101" H 1950 1700 50  0001 C CNN
F 1 "GND" H 1950 1800 50  0000 C CNN
F 2 "" H 1950 1950 50  0000 C CNN
F 3 "" H 1950 1950 50  0000 C CNN
	1    1950 1950
	1    0    0    -1  
$EndComp
$Sheet
S 2650 1550 1200 1450
U 59EFEA3A
F0 "Power" 60
F1 "pbv3_power.sch" 60
F2 "VOUT" I R 3850 1650 60 
F3 "VIN" I L 2650 1650 60 
F4 "VIN_RTN" I L 2650 1800 60 
F5 "PTAT" I R 3850 2900 60 
F6 "DCDCEN" I L 2650 2350 60 
F7 "PGOOD" I R 3850 2750 60 
F8 "FVOUT" I R 3850 2450 60 
F9 "VCC3V3" I R 3850 1950 60 
F10 "VCC1V4" I R 3850 1800 60 
F11 "LREGEN" I L 2650 2200 60 
F12 "NTCp" I L 2650 2750 60 
F13 "NTCn" I L 2650 2900 60 
F14 "FVIN" I R 3850 2600 60 
F15 "Bit1_VOUT" I R 3850 2150 60 
F16 "Bit2_VOUT" I R 3850 2300 60 
$EndSheet
$Comp
L pbv3:BondPad J13
U 1 1 59EEFF0F
P 5800 1350
F 0 "J13" H 5800 1450 60  0000 C CNN
F 1 "VOUT_RTN_X" H 5800 1250 60  0000 C CNN
F 2 "pbv3:pad" H 5800 1350 60  0001 C CNN
F 3 "" H 5800 1350 60  0001 C CNN
	1    5800 1350
	-1   0    0    1   
$EndComp
$Comp
L pbv3:BondPad J11
U 1 1 59EEFE53
P 5100 1350
F 0 "J11" H 5100 1450 60  0000 C CNN
F 1 "VOUT_X" H 5100 1250 60  0000 C CNN
F 2 "pbv3:pad" H 5100 1350 60  0001 C CNN
F 3 "" H 5100 1350 60  0001 C CNN
	1    5100 1350
	-1   0    0    1   
$EndComp
$Comp
L pbv3:BondPad J2
U 1 1 59EEE968
P 1550 1900
F 0 "J2" H 1550 2000 60  0000 C CNN
F 1 "VIN_RTN" H 1550 1800 60  0000 C CNN
F 2 "pbv3:pad" H 1550 1900 60  0001 C CNN
F 3 "" H 1550 1900 60  0001 C CNN
	1    1550 1900
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J1
U 1 1 59EEE934
P 1550 1550
F 0 "J1" H 1550 1650 60  0000 C CNN
F 1 "VIN" H 1550 1450 60  0000 C CNN
F 2 "pbv3:pad" H 1550 1550 60  0001 C CNN
F 3 "" H 1550 1550 60  0001 C CNN
	1    1550 1550
	1    0    0    -1  
$EndComp
Text Notes 7100 8050 0    260  ~ 0
AMAC
$Sheet
S 2800 12700 1050 1000
U 5CFFA990
F0 "sheet5CFFA985" 60
F1 "hv.sch" 60
F2 "HVOSC" I L 2800 12850 60 
F3 "GND" I L 2800 13000 60 
F4 "HV_IN" I L 2800 13300 60 
F5 "HV_IN_RTN" I L 2800 13450 60 
F6 "HV_OUT_RTN" I R 3850 13200 60 
F7 "HV_OUT" I R 3850 13050 60 
$EndSheet
Text Notes 5550 8050 0    260  ~ 0
POWER
Text Notes 6250 12050 0    260  ~ 0
HV
$Comp
L pbv3:BondPad J101
U 1 1 5CFFA998
P 4800 13050
F 0 "J101" H 4800 13150 60  0000 C CNN
F 1 "HV_OUT2" H 4800 12950 60  0000 C CNN
F 2 "pbv3:pad" H 4800 13050 60  0001 C CNN
F 3 "" H 4800 13050 60  0001 C CNN
	1    4800 13050
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5CFFA99E
P 2550 13100
F 0 "#PWR0110" H 2550 12850 50  0001 C CNN
F 1 "GND" H 2550 12950 50  0000 C CNN
F 2 "" H 2550 13100 50  0000 C CNN
F 3 "" H 2550 13100 50  0000 C CNN
	1    2550 13100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5CFFA9A4
P 7650 9250
F 0 "#PWR0113" H 7650 9000 50  0001 C CNN
F 1 "GND" H 7650 9100 50  0000 C CNN
F 2 "" H 7650 9250 50  0000 C CNN
F 3 "" H 7650 9250 50  0000 C CNN
	1    7650 9250
	1    0    0    -1  
$EndComp
Text Label 7900 9550 0    60   ~ 0
FVOUT2
Text Label 7900 9800 0    60   ~ 0
FVIN2
Text Label 7900 9450 0    60   ~ 0
VOUT2
Text Label 7900 9700 0    60   ~ 0
VIN
Text Label 9750 8950 0    60   ~ 0
DCDCEN2
Text Label 9750 9050 0    60   ~ 0
PGOOD2
Text Label 9750 9150 0    60   ~ 0
PTAT2
Text Label 9750 9250 0    60   ~ 0
NTCPBn2
Text Label 9750 9350 0    60   ~ 0
NTCPBp2
$Comp
L pbv3:BondPad J107
U 1 1 5CFFA9B3
P 7950 10250
F 0 "J107" H 7950 10350 60  0000 C CNN
F 1 "HRSTBx2" H 7650 10250 60  0000 C CNN
F 2 "pbv3:pad" H 7950 10250 60  0001 C CNN
F 3 "" H 7950 10250 60  0001 C CNN
	1    7950 10250
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J108
U 1 1 5CFFA9B9
P 7950 10450
F 0 "J108" H 7950 10550 60  0000 C CNN
F 1 "LDx0EN2" H 7650 10450 60  0000 C CNN
F 2 "pbv3:pad" H 7950 10450 60  0001 C CNN
F 3 "" H 7950 10450 60  0001 C CNN
	1    7950 10450
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J109
U 1 1 5CFFA9BF
P 7950 10650
F 0 "J109" H 7950 10750 60  0000 C CNN
F 1 "HREFx2" H 7650 10650 60  0000 C CNN
F 2 "pbv3:pad" H 7950 10650 60  0001 C CNN
F 3 "" H 7950 10650 60  0001 C CNN
	1    7950 10650
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J110
U 1 1 5CFFA9C5
P 7950 10850
F 0 "J110" H 7950 10950 60  0000 C CNN
F 1 "NTCxP2" H 7650 10850 60  0000 C CNN
F 2 "pbv3:pad" H 7950 10850 60  0001 C CNN
F 3 "" H 7950 10850 60  0001 C CNN
	1    7950 10850
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J111
U 1 1 5CFFA9CB
P 7950 11050
F 0 "J111" H 7950 11150 60  0000 C CNN
F 1 "NTCxN2" H 7650 11050 60  0000 C CNN
F 2 "pbv3:pad" H 7950 11050 60  0001 C CNN
F 3 "" H 7950 11050 60  0001 C CNN
	1    7950 11050
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J112
U 1 1 5CFFA9D1
P 7950 11250
F 0 "J112" H 7950 11350 60  0000 C CNN
F 1 "LDx1EN2" H 7650 11250 60  0000 C CNN
F 2 "pbv3:pad" H 7950 11250 60  0001 C CNN
F 3 "" H 7950 11250 60  0001 C CNN
	1    7950 11250
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J106
U 1 1 5CFFA9D7
P 7950 10050
F 0 "J106" H 7950 10150 60  0000 C CNN
F 1 "SSSHx2" H 7650 10050 60  0000 C CNN
F 2 "pbv3:pad" H 7950 10050 60  0001 C CNN
F 3 "" H 7950 10050 60  0001 C CNN
	1    7950 10050
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J115
U 1 1 5CFFA9DD
P 7950 11850
F 0 "J115" H 7950 11950 60  0000 C CNN
F 1 "CALx2" H 7650 11850 60  0000 C CNN
F 2 "pbv3:pad" H 7950 11850 60  0001 C CNN
F 3 "" H 7950 11850 60  0001 C CNN
	1    7950 11850
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J114
U 1 1 5CFFA9E3
P 7950 11650
F 0 "J114" H 7950 11750 60  0000 C CNN
F 1 "SHUNTx2" H 7650 11650 60  0000 C CNN
F 2 "pbv3:pad" H 7950 11650 60  0001 C CNN
F 3 "" H 7950 11650 60  0001 C CNN
	1    7950 11650
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J113
U 1 1 5CFFA9E9
P 7950 11450
F 0 "J113" H 7950 11550 60  0000 C CNN
F 1 "LDx2EN2" H 7650 11450 60  0000 C CNN
F 2 "pbv3:pad" H 7950 11450 60  0001 C CNN
F 3 "" H 7950 11450 60  0001 C CNN
	1    7950 11450
	1    0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J118
U 1 1 5CFFA9EF
P 9750 10250
F 0 "J118" H 9750 10350 60  0000 C CNN
F 1 "HRSTBy2" H 9450 10250 60  0000 C CNN
F 2 "pbv3:pad" H 9750 10250 60  0001 C CNN
F 3 "" H 9750 10250 60  0001 C CNN
	1    9750 10250
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J119
U 1 1 5CFFA9F5
P 9750 10450
F 0 "J119" H 9750 10550 60  0000 C CNN
F 1 "LDy0EN2" H 9450 10450 60  0000 C CNN
F 2 "pbv3:pad" H 9750 10450 60  0001 C CNN
F 3 "" H 9750 10450 60  0001 C CNN
	1    9750 10450
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J120
U 1 1 5CFFA9FB
P 9750 10650
F 0 "J120" H 9750 10750 60  0000 C CNN
F 1 "HREFy2" H 9450 10650 60  0000 C CNN
F 2 "pbv3:pad" H 9750 10650 60  0001 C CNN
F 3 "" H 9750 10650 60  0001 C CNN
	1    9750 10650
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J121
U 1 1 5CFFAA01
P 9750 10850
F 0 "J121" H 9750 10950 60  0000 C CNN
F 1 "NTCyN2" H 9450 10850 60  0000 C CNN
F 2 "pbv3:pad" H 9750 10850 60  0001 C CNN
F 3 "" H 9750 10850 60  0001 C CNN
	1    9750 10850
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J122
U 1 1 5CFFAA07
P 9750 11050
F 0 "J122" H 9750 11150 60  0000 C CNN
F 1 "NTCyP2" H 9450 11050 60  0000 C CNN
F 2 "pbv3:pad" H 9750 11050 60  0001 C CNN
F 3 "" H 9750 11050 60  0001 C CNN
	1    9750 11050
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J123
U 1 1 5CFFAA0D
P 9750 11250
F 0 "J123" H 9750 11350 60  0000 C CNN
F 1 "LDy1EN2" H 9450 11250 60  0000 C CNN
F 2 "pbv3:pad" H 9750 11250 60  0001 C CNN
F 3 "" H 9750 11250 60  0001 C CNN
	1    9750 11250
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J117
U 1 1 5CFFAA13
P 9750 10050
F 0 "J117" H 9750 10150 60  0000 C CNN
F 1 "SSSHy2" H 9450 10050 60  0000 C CNN
F 2 "pbv3:pad" H 9750 10050 60  0001 C CNN
F 3 "" H 9750 10050 60  0001 C CNN
	1    9750 10050
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J126
U 1 1 5CFFAA19
P 9750 11850
F 0 "J126" H 9750 11950 60  0000 C CNN
F 1 "CAly2" H 9450 11850 60  0000 C CNN
F 2 "pbv3:pad" H 9750 11850 60  0001 C CNN
F 3 "" H 9750 11850 60  0001 C CNN
	1    9750 11850
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J125
U 1 1 5CFFAA1F
P 9750 11650
F 0 "J125" H 9750 11750 60  0000 C CNN
F 1 "SHUNTy2" H 9450 11650 60  0000 C CNN
F 2 "pbv3:pad" H 9750 11650 60  0001 C CNN
F 3 "" H 9750 11650 60  0001 C CNN
	1    9750 11650
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J124
U 1 1 5CFFAA25
P 9750 11450
F 0 "J124" H 9750 11550 60  0000 C CNN
F 1 "LDy2EN2" H 9450 11450 60  0000 C CNN
F 2 "pbv3:pad" H 9750 11450 60  0001 C CNN
F 3 "" H 9750 11450 60  0001 C CNN
	1    9750 11450
	-1   0    0    -1  
$EndComp
$Comp
L pbv3:BondPad J127
U 1 1 5CFFAA2B
P 9750 12150
F 0 "J127" H 9750 12250 60  0000 C CNN
F 1 "HVretY2" H 9450 12150 60  0000 C CNN
F 2 "pbv3:pad" H 9750 12150 60  0001 C CNN
F 3 "" H 9750 12150 60  0001 C CNN
	1    9750 12150
	-1   0    0    1   
$EndComp
$Comp
L pbv3:BondPad J128
U 1 1 5CFFAA4F
P 9750 13150
F 0 "J128" H 9750 13250 60  0000 C CNN
F 1 "OFout" H 9450 13150 60  0000 C CNN
F 2 "pbv3:pad" H 9750 13150 60  0001 C CNN
F 3 "" H 9750 13150 60  0001 C CNN
	1    9750 13150
	-1   0    0    1   
$EndComp
Text Notes 7500 9800 0    60   ~ 0
50mOhm
Text Notes 7500 9550 0    60   ~ 0
10mOhm
Text Label 7900 13150 0    60   ~ 0
OFcom
Text Label 9750 9550 0    60   ~ 0
GPO12
Text Label 9750 9450 0    60   ~ 0
GPO22
$Comp
L power:GND #PWR0109
U 1 1 5CFFAA61
P 2350 13550
F 0 "#PWR0109" H 2350 13300 50  0001 C CNN
F 1 "GND" H 2350 13400 50  0000 C CNN
F 2 "" H 2350 13550 50  0000 C CNN
F 3 "" H 2350 13550 50  0000 C CNN
	1    2350 13550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5CFFAA6D
P 4200 13300
F 0 "#PWR0111" H 4200 13050 50  0001 C CNN
F 1 "GND" H 4200 13150 50  0000 C CNN
F 2 "" H 4200 13300 50  0000 C CNN
F 3 "" H 4200 13300 50  0000 C CNN
	1    4200 13300
	1    0    0    -1  
$EndComp
Wire Notes Line
	500  7700 6900 7700
Wire Notes Line
	6900 11600 500  11600
Wire Notes Line
	500  11600 500  7700
Wire Wire Line
	4200 13300 4200 13200
Wire Notes Line
	500  11700 6800 11700
Wire Notes Line
	6800 11700 6800 14800
Wire Notes Line
	6800 14800 500  14800
Wire Notes Line
	500  14800 500  11700
Wire Notes Line
	7000 7700 11000 7700
Wire Notes Line
	11000 7700 11000 13500
Wire Notes Line
	11000 13500 7000 13500
Wire Notes Line
	7000 13500 7000 7700
Wire Notes Line
	6900 7700 6900 11600
Wire Wire Line
	4200 13200 3850 13200
Wire Wire Line
	3850 13050 4550 13050
Wire Wire Line
	2800 13000 2550 13000
Wire Wire Line
	2550 13000 2550 13100
Wire Wire Line
	2800 13450 2350 13450
Wire Wire Line
	7650 9250 8350 9250
Wire Wire Line
	7900 9550 8350 9550
Wire Wire Line
	7900 9800 8350 9800
Wire Wire Line
	7900 9450 8350 9450
Wire Wire Line
	7900 9700 8350 9700
Wire Wire Line
	9350 8950 9750 8950
Wire Wire Line
	9350 9050 9750 9050
Wire Wire Line
	9350 9150 9750 9150
Wire Wire Line
	9350 9250 9750 9250
Wire Wire Line
	9350 9350 9750 9350
Wire Wire Line
	7300 12450 2500 12450
Wire Wire Line
	2500 12450 2500 12850
Wire Wire Line
	2500 12850 2800 12850
Wire Wire Line
	7300 12450 7300 12350
Wire Wire Line
	7300 12350 8350 12350
Wire Wire Line
	8200 10050 8350 10050
Wire Wire Line
	8200 10250 8350 10250
Wire Wire Line
	8200 10450 8350 10450
Wire Wire Line
	8200 10650 8350 10650
Wire Wire Line
	8200 10850 8350 10850
Wire Wire Line
	8200 11050 8350 11050
Wire Wire Line
	8200 11250 8350 11250
Wire Wire Line
	9350 10050 9500 10050
Wire Wire Line
	9350 10250 9500 10250
Wire Wire Line
	9350 10450 9500 10450
Wire Wire Line
	9350 10650 9500 10650
Wire Wire Line
	9350 10850 9500 10850
Wire Wire Line
	9350 11050 9500 11050
Wire Wire Line
	9350 11250 9500 11250
Wire Wire Line
	9350 11450 9500 11450
Wire Wire Line
	9350 11650 9500 11650
Wire Wire Line
	9350 11850 9500 11850
Wire Wire Line
	9350 12150 9500 12150
Wire Wire Line
	9350 12600 9450 12600
Wire Wire Line
	9450 12600 9450 12550
Wire Wire Line
	9350 12700 9450 12700
Wire Wire Line
	9450 12700 9450 12750
Wire Wire Line
	9350 13150 9500 13150
Wire Wire Line
	8200 11450 8350 11450
Wire Wire Line
	8200 11650 8350 11650
Wire Wire Line
	8200 11850 8350 11850
Wire Wire Line
	8350 13150 7900 13150
Wire Wire Line
	9350 9450 9750 9450
Wire Wire Line
	9350 9550 9750 9550
Wire Wire Line
	2350 13450 2350 13550
Wire Wire Line
	8100 9050 8100 8600
Wire Wire Line
	8100 8600 10350 8600
Wire Wire Line
	10350 8600 10350 13000
Wire Wire Line
	10350 13000 9350 13000
Text Label 8150 8600 0    60   ~ 0
VCC1V42
Wire Wire Line
	8100 9050 8350 9050
$Sheet
S 8350 8850 1000 4450
U 5CFFAAEF
F0 "sheet5CFFA986" 60
F1 "amac2.sch" 60
F2 "VDCDC" I L 8350 8950 60 
F3 "VLREG" I L 8350 9050 60 
F4 "GND" I L 8350 9250 60 
F5 "CUR1Vp" I L 8350 9450 60 
F6 "CUR1Vn" I L 8350 9550 60 
F7 "DCDCEN" I R 9350 8950 60 
F8 "NTCPBn" I R 9350 9250 60 
F9 "NTCPBp" I R 9350 9350 60 
F10 "CUR10Vn" I L 8350 9800 60 
F11 "CUR10Vp" I L 8350 9700 60 
F12 "PGOOD" I R 9350 9050 60 
F13 "PTAT" I R 9350 9150 60 
F14 "SSSHy" I R 9350 10050 60 
F15 "HRSTBy" I R 9350 10250 60 
F16 "LDy0EN" I R 9350 10450 60 
F17 "HREFy" I R 9350 10650 60 
F18 "NTCyN" I R 9350 10850 60 
F19 "NTCyP" I R 9350 11050 60 
F20 "LDy1EN" I R 9350 11250 60 
F21 "LDy2EN" I R 9350 11450 60 
F22 "SHUNTy" I R 9350 11650 60 
F23 "CALy" I R 9350 11850 60 
F24 "OFout" I R 9350 13150 60 
F25 "GPO2" I R 9350 9450 60 
F26 "HVret1" I L 8350 12150 60 
F27 "CMDin*_P" I L 8350 12600 60 
F28 "CMDin*_N" I L 8350 12700 60 
F29 "CMDout_P" O R 9350 12600 60 
F30 "CMDout_N" I R 9350 12700 60 
F31 "HVOSC0" I L 8350 12350 60 
F32 "VDDHI" I L 8350 9150 60 
F33 "LAM" I R 9350 13000 60 
F34 "HVret2" I R 9350 12150 60 
F35 "LDx0EN" I L 8350 10450 60 
F36 "HRSTBx" I L 8350 10250 60 
F37 "SSSHx" I L 8350 10050 60 
F38 "HREFx" I L 8350 10650 60 
F39 "NTCxP" I L 8350 10850 60 
F40 "NTCxN" I L 8350 11050 60 
F41 "LDx1EN" I L 8350 11250 60 
F42 "LDx2EN" I L 8350 11450 60 
F43 "SHUNTx" I L 8350 11650 60 
F44 "CALx" I L 8350 11850 60 
F45 "OFin" I L 8350 13150 60 
F46 "CAL" I L 8350 13000 60 
F47 "GPO1" I R 9350 9550 60 
$EndSheet
Connection ~ 8100 9050
Wire Wire Line
	5450 9150 8350 9150
Wire Wire Line
	5150 9050 8100 9050
Wire Wire Line
	4150 8800 4150 8950
Wire Wire Line
	5500 8650 5500 8800
Wire Wire Line
	4800 8650 4800 8800
Connection ~ 5450 9150
Wire Wire Line
	5450 9250 5450 9150
Connection ~ 5150 9050
Wire Wire Line
	5150 9250 5150 9050
Wire Wire Line
	3800 9300 4350 9300
Wire Wire Line
	3800 9450 4350 9450
Wire Wire Line
	2600 10050 2000 10050
Wire Wire Line
	2600 9900 2000 9900
Wire Wire Line
	3800 10050 4350 10050
Wire Wire Line
	3800 9900 4350 9900
Wire Wire Line
	3800 9750 4350 9750
Wire Wire Line
	3800 9600 4350 9600
Wire Wire Line
	4000 9150 5450 9150
Wire Wire Line
	4000 9100 4000 9150
Wire Wire Line
	3800 9100 4000 9100
Wire Wire Line
	4050 9050 5150 9050
Wire Wire Line
	4050 8950 4050 9050
Wire Wire Line
	3800 8950 4050 8950
Connection ~ 4150 8800
Wire Wire Line
	4150 8950 8350 8950
Wire Wire Line
	1900 8950 2600 8950
Wire Wire Line
	5350 8650 5350 8750
Wire Wire Line
	5500 8650 5350 8650
Wire Wire Line
	4150 8650 4800 8650
Wire Wire Line
	4150 8650 4150 8800
Wire Wire Line
	3800 8800 4150 8800
$Comp
L pbv3:BondPad J104
U 1 1 5CFFAB25
P 5450 9500
F 0 "J104" H 5450 9600 60  0000 C CNN
F 1 "T3V32" H 5450 9400 60  0000 C CNN
F 2 "pbv3:pad" H 5450 9500 60  0001 C CNN
F 3 "" H 5450 9500 60  0001 C CNN
	1    5450 9500
	0    -1   -1   0   
$EndComp
$Comp
L pbv3:BondPad J103
U 1 1 5CFFAB2B
P 5150 9500
F 0 "J103" H 5150 9600 60  0000 C CNN
F 1 "T1V42" H 5150 9400 60  0000 C CNN
F 2 "pbv3:pad" H 5150 9500 60  0001 C CNN
F 3 "" H 5150 9500 60  0001 C CNN
	1    5150 9500
	0    -1   -1   0   
$EndComp
Text Label 4350 9300 0    60   ~ 0
GPO12
Text Label 4350 9450 0    60   ~ 0
GPO22
Text Notes 1800 8500 0    60   ~ 0
Fuse?
Text Label 2000 10050 0    60   ~ 0
NTCPBn2
Text Label 2000 9900 0    60   ~ 0
NTCPBp2
Text Label 2000 9500 0    60   ~ 0
DCDCEN2
Text Label 4350 10050 0    60   ~ 0
PTAT2
Text Label 4350 9900 0    60   ~ 0
PGOOD2
Text Label 4350 9750 0    60   ~ 0
FVIN2
Text Label 4350 9600 0    60   ~ 0
FVOUT2
Text Label 2150 8800 0    60   ~ 0
VIN
Text Label 4250 8650 0    60   ~ 0
VOUT2
$Comp
L power:GND #PWR0108
U 1 1 5CFFAB44
P 1900 9100
F 0 "#PWR0108" H 1900 8850 50  0001 C CNN
F 1 "GND" H 1900 8950 50  0000 C CNN
F 2 "" H 1900 9100 50  0000 C CNN
F 3 "" H 1900 9100 50  0000 C CNN
	1    1900 9100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5CFFAB4A
P 5350 8750
F 0 "#PWR0112" H 5350 8500 50  0001 C CNN
F 1 "GND" H 5350 8600 50  0000 C CNN
F 2 "" H 5350 8750 50  0000 C CNN
F 3 "" H 5350 8750 50  0000 C CNN
	1    5350 8750
	1    0    0    -1  
$EndComp
$Sheet
S 2600 8700 1200 1450
U 5CFFAB5B
F0 "sheet5CFFA987" 60
F1 "pbv3_power.sch" 60
F2 "VOUT" I R 3800 8800 60 
F3 "VIN" I L 2600 8800 60 
F4 "VIN_RTN" I L 2600 8950 60 
F5 "PTAT" I R 3800 10050 60 
F6 "DCDCEN" I L 2600 9500 60 
F7 "PGOOD" I R 3800 9900 60 
F8 "FVOUT" I R 3800 9600 60 
F9 "VCC3V3" I R 3800 9100 60 
F10 "VCC1V4" I R 3800 8950 60 
F11 "LREGEN" I L 2600 9350 60 
F12 "NTCp" I L 2600 9900 60 
F13 "NTCn" I L 2600 10050 60 
F14 "FVIN" I R 3800 9750 60 
F15 "Bit1_VOUT" I R 3800 9300 60 
F16 "Bit2_VOUT" I R 3800 9450 60 
$EndSheet
$Comp
L pbv3:BondPad J105
U 1 1 5CFFAB61
P 5750 8800
F 0 "J105" H 5750 8900 60  0000 C CNN
F 1 "VOUT_RTN_Y" H 5750 8700 60  0000 C CNN
F 2 "pbv3:pad" H 5750 8800 60  0001 C CNN
F 3 "" H 5750 8800 60  0001 C CNN
	1    5750 8800
	-1   0    0    1   
$EndComp
$Comp
L pbv3:BondPad J102
U 1 1 5CFFAB6D
P 5050 8800
F 0 "J102" H 5050 8900 60  0000 C CNN
F 1 "VOUT_Y" H 5050 8700 60  0000 C CNN
F 2 "pbv3:pad" H 5050 8800 60  0001 C CNN
F 3 "" H 5050 8800 60  0001 C CNN
	1    5050 8800
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 59F05EAA
P 5400 1600
F 0 "#PWR0105" H 5400 1350 50  0001 C CNN
F 1 "GND" H 5400 1450 50  0000 C CNN
F 2 "" H 5400 1600 50  0000 C CNN
F 3 "" H 5400 1600 50  0000 C CNN
	1    5400 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 1500 5400 1600
Wire Wire Line
	1900 8950 1900 9100
Wire Wire Line
	2600 8800 2150 8800
Wire Wire Line
	2600 9500 2000 9500
Wire Wire Line
	2600 9350 1250 9350
Wire Wire Line
	1250 9350 1250 8050
Wire Wire Line
	1250 8050 10100 8050
Wire Wire Line
	10100 8050 10100 6000
Wire Wire Line
	10100 6000 9400 6000
Text Label 2000 9350 0    60   ~ 0
OFcom
Text Label 7900 13000 0    60   ~ 0
CAL
Wire Wire Line
	7900 13000 8350 13000
Text Label 7950 5750 0    60   ~ 0
CAL
Wire Wire Line
	7850 5750 8400 5750
Wire Wire Line
	7850 5400 8300 5400
Wire Wire Line
	7850 5600 8300 5600
Text Label 7900 5400 0    60   ~ 0
CMDinP
Text Label 7900 5600 0    60   ~ 0
CMDinN
Wire Wire Line
	9500 5400 9900 5400
Wire Wire Line
	9500 5600 9900 5600
Text Label 9550 5400 0    60   ~ 0
CMDoutP
Text Label 9550 5600 0    60   ~ 0
CMDoutN
Text Label 9600 12550 0    60   ~ 0
CMDoutP
Text Label 9600 12750 0    60   ~ 0
CMDoutN
Wire Wire Line
	9450 12550 9600 12550
Wire Wire Line
	9450 12750 9600 12750
Text Label 2150 13300 0    60   ~ 0
HVin
Wire Wire Line
	2150 13300 2800 13300
Text Label 2300 6050 0    60   ~ 0
HVin
Wire Wire Line
	8400 5900 7950 5900
Wire Wire Line
	8350 12600 7200 12600
Wire Wire Line
	7200 12600 7200 6000
Wire Wire Line
	7200 6000 8400 6000
Wire Wire Line
	8400 6100 7250 6100
Wire Wire Line
	7250 6100 7250 12700
Wire Wire Line
	7250 12700 8350 12700
NoConn ~ 8350 12150
NoConn ~ 9400 5000
$EndSCHEMATC
