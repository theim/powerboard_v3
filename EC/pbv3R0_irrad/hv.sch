EESchema Schematic File Version 2
LIBS:pbv3
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:pbv3-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title "Powerboard v3"
Date "2017-10-23"
Rev "A"
Comp "LBNL"
Comment1 "Timon Heim"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L R R14
U 1 1 59F08275
P 3150 3500
F 0 "R14" V 3230 3500 50  0000 C CNN
F 1 "500" V 3150 3500 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 3080 3500 50  0001 C CNN
F 3 "" H 3150 3500 50  0000 C CNN
	1    3150 3500
	0    1    1    0   
$EndComp
$Comp
L C C19
U 1 1 59F0827C
P 3500 3650
F 0 "C19" H 3525 3750 50  0000 L CNN
F 1 "1nF" H 3525 3550 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3538 3500 50  0001 C CNN
F 3 "" H 3500 3650 50  0000 C CNN
	1    3500 3650
	1    0    0    -1  
$EndComp
$Comp
L C C20
U 1 1 59F08283
P 3850 3500
F 0 "C20" H 3875 3600 50  0000 L CNN
F 1 "47nF/1kV" H 3875 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 3888 3350 50  0001 C CNN
F 3 "" H 3850 3500 50  0000 C CNN
	1    3850 3500
	0    1    1    0   
$EndComp
$Comp
L BAS70DW-04 D1
U 1 1 59F0828A
P 4600 4100
F 0 "D1" H 4600 4500 60  0000 C CNN
F 1 "BAS70DW-04" H 4600 3600 60  0000 C CNN
F 2 "TO_SOT_Packages_SMD:SOT-363_SC-70-6" V 4600 4100 60  0001 C CNN
F 3 "" V 4600 4100 60  0001 C CNN
	1    4600 4100
	0    1    1    0   
$EndComp
$Comp
L C C22
U 1 1 59F08291
P 4400 4700
F 0 "C22" H 4425 4800 50  0000 L CNN
F 1 "1uF" H 4425 4600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4438 4550 50  0001 C CNN
F 3 "" H 4400 4700 50  0000 C CNN
	1    4400 4700
	0    1    1    0   
$EndComp
$Comp
L C C23
U 1 1 59F08298
P 4700 3500
F 0 "C23" H 4725 3600 50  0000 L CNN
F 1 "1uF" H 4725 3400 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 4738 3350 50  0001 C CNN
F 3 "" H 4700 3500 50  0000 C CNN
	1    4700 3500
	0    1    1    0   
$EndComp
$Comp
L C C24
U 1 1 59F0829F
P 5200 4100
F 0 "C24" H 5225 4200 50  0000 L CNN
F 1 "1uF" H 5225 4000 50  0000 L CNN
F 2 "Capacitors_SMD:C_0603" H 5238 3950 50  0001 C CNN
F 3 "" H 5200 4100 50  0000 C CNN
	1    5200 4100
	1    0    0    -1  
$EndComp
$Comp
L C C21
U 1 1 59F082A6
P 3850 4900
F 0 "C21" H 3875 5000 50  0000 L CNN
F 1 "47nF/1kV" H 3875 4800 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 3888 4750 50  0001 C CNN
F 3 "" H 3850 4900 50  0000 C CNN
	1    3850 4900
	0    1    1    0   
$EndComp
Wire Wire Line
	3000 3500 2700 3500
Wire Wire Line
	3300 3500 3700 3500
Connection ~ 3500 3500
Wire Wire Line
	4000 3500 4250 3500
Wire Wire Line
	4250 3300 4250 3600
Wire Wire Line
	4850 3600 4850 3500
Wire Wire Line
	4250 4600 4250 4700
Wire Wire Line
	4550 4600 4550 4800
Wire Wire Line
	4250 4700 4150 4700
Wire Wire Line
	4150 4700 4150 4900
Wire Wire Line
	4000 4900 5450 4900
$Comp
L GND #PWR018
U 1 1 59F082B8
P 3500 3900
F 0 "#PWR018" H 3500 3650 50  0001 C CNN
F 1 "GND" H 3500 3750 50  0000 C CNN
F 2 "" H 3500 3900 50  0000 C CNN
F 3 "" H 3500 3900 50  0000 C CNN
	1    3500 3900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR019
U 1 1 59F082BE
P 3450 4950
F 0 "#PWR019" H 3450 4700 50  0001 C CNN
F 1 "GND" H 3450 4800 50  0000 C CNN
F 2 "" H 3450 4950 50  0000 C CNN
F 3 "" H 3450 4950 50  0000 C CNN
	1    3450 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 3900 3500 3800
Wire Wire Line
	2700 4900 3700 4900
Wire Wire Line
	3450 4900 3450 4950
Wire Wire Line
	4850 4600 4850 4700
Wire Wire Line
	4850 4700 5200 4700
Wire Wire Line
	5200 4700 5200 4250
Wire Wire Line
	5200 3500 5200 3950
Wire Wire Line
	4850 3500 5200 3500
Wire Wire Line
	4550 3200 4550 3600
Wire Wire Line
	4250 3300 5350 3300
Wire Wire Line
	5350 3300 5350 4800
Wire Wire Line
	5350 4800 4550 4800
Wire Wire Line
	4550 4700 4550 4700
Connection ~ 4250 3500
Wire Wire Line
	4550 3200 5450 3200
Connection ~ 4550 3500
Connection ~ 4150 4900
$Comp
L R R15
U 1 1 59F082D5
P 5700 3850
F 0 "R15" V 5780 3850 50  0000 C CNN
F 1 "100" V 5700 3850 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 5630 3850 50  0001 C CNN
F 3 "" H 5700 3850 50  0000 C CNN
	1    5700 3850
	0    1    1    0   
$EndComp
$Comp
L R R16
U 1 1 59F082DC
P 5700 4250
F 0 "R16" V 5780 4250 50  0000 C CNN
F 1 "0" V 5700 4250 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 5630 4250 50  0001 C CNN
F 3 "" H 5700 4250 50  0000 C CNN
	1    5700 4250
	0    1    1    0   
$EndComp
$Comp
L C C25
U 1 1 59F082E3
P 6000 4050
F 0 "C25" H 6025 4150 50  0000 L CNN
F 1 "10nF" H 6025 3950 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 6038 3900 50  0001 C CNN
F 3 "" H 6000 4050 50  0000 C CNN
	1    6000 4050
	1    0    0    -1  
$EndComp
$Comp
L Q_NMOS_DGS Q1
U 1 1 59F082EA
P 6800 3850
F 0 "Q1" H 7100 3900 50  0000 R CNN
F 1 "HVmux" H 6850 4050 50  0000 R CNN
F 2 "pbv3:HVmux" H 7000 3950 50  0001 C CNN
F 3 "" H 6800 3850 50  0000 C CNN
	1    6800 3850
	1    0    0    -1  
$EndComp
$Comp
L R R17
U 1 1 59F082F1
P 6550 4050
F 0 "R17" V 6630 4050 50  0000 C CNN
F 1 "20k" V 6550 4050 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" V 6480 4050 50  0001 C CNN
F 3 "" H 6550 4050 50  0000 C CNN
	1    6550 4050
	1    0    0    -1  
$EndComp
$Comp
L D_Zener D2
U 1 1 59F082F8
P 6350 4050
F 0 "D2" H 6350 4150 50  0000 C CNN
F 1 "VDZT2R4.7B" H 6350 3950 50  0000 C CNN
F 2 "Diodes_SMD:D_0603" H 6350 4050 50  0001 C CNN
F 3 "" H 6350 4050 50  0000 C CNN
	1    6350 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	5850 3850 6600 3850
Wire Wire Line
	6000 3850 6000 3900
Wire Wire Line
	5850 4250 6900 4250
Wire Wire Line
	6000 4250 6000 4200
Connection ~ 6000 4250
Connection ~ 6000 3850
Wire Wire Line
	6900 4050 6900 4500
Connection ~ 6900 4250
Connection ~ 6350 3850
Connection ~ 6350 4250
Wire Wire Line
	6550 4200 6550 4250
Connection ~ 6550 4250
Wire Wire Line
	6550 3900 6550 3850
Connection ~ 6550 3850
$Comp
L R R18
U 1 1 59F08314
P 7300 2900
F 0 "R18" V 7380 2900 50  0000 C CNN
F 1 "5M" V 7300 2900 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 7230 2900 50  0001 C CNN
F 3 "" H 7300 2900 50  0000 C CNN
	1    7300 2900
	1    0    0    -1  
$EndComp
$Comp
L R R19
U 1 1 59F0831B
P 7300 3300
F 0 "R19" V 7380 3300 50  0000 C CNN
F 1 "5M" V 7300 3300 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 7230 3300 50  0001 C CNN
F 3 "" H 7300 3300 50  0000 C CNN
	1    7300 3300
	1    0    0    -1  
$EndComp
$Comp
L R R20
U 1 1 59F08322
P 7650 2700
F 0 "R20" V 7730 2700 50  0000 C CNN
F 1 "5k" V 7650 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7580 2700 50  0001 C CNN
F 3 "" H 7650 2700 50  0000 C CNN
	1    7650 2700
	0    1    1    0   
$EndComp
$Comp
L R R21
U 1 1 59F08329
P 8050 2700
F 0 "R21" V 8130 2700 50  0000 C CNN
F 1 "5k" V 8050 2700 50  0000 C CNN
F 2 "Resistors_SMD:R_0603" V 7980 2700 50  0001 C CNN
F 3 "" H 8050 2700 50  0000 C CNN
	1    8050 2700
	0    1    1    0   
$EndComp
$Comp
L C C26
U 1 1 59F08330
P 7850 3100
F 0 "C26" H 7875 3200 50  0000 L CNN
F 1 "22nF/1kV" H 7875 3000 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 7888 2950 50  0001 C CNN
F 3 "" H 7850 3100 50  0000 C CNN
	1    7850 3100
	1    0    0    -1  
$EndComp
$Comp
L C C27
U 1 1 59F08337
P 8250 3100
F 0 "C27" H 8275 3200 50  0000 L CNN
F 1 "22nF/1kV" H 8275 3000 50  0000 L CNN
F 2 "Capacitors_SMD:C_1210" H 8288 2950 50  0001 C CNN
F 3 "" H 8250 3100 50  0000 C CNN
	1    8250 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 2700 6900 3650
Wire Wire Line
	6900 2700 7500 2700
Wire Wire Line
	7300 2750 7300 2700
Connection ~ 7300 2700
Wire Wire Line
	7300 3150 7300 3050
Wire Wire Line
	7300 3450 7300 3500
Wire Wire Line
	7200 3500 8450 3500
Wire Wire Line
	8250 3500 8250 3250
Wire Wire Line
	7850 3250 7850 3500
Connection ~ 7850 3500
Wire Wire Line
	7800 2700 7900 2700
Wire Wire Line
	7850 2950 7850 2700
Connection ~ 7850 2700
Wire Wire Line
	8250 2950 8250 2700
Wire Wire Line
	8200 2700 8450 2700
Connection ~ 8250 2700
Wire Wire Line
	7200 4500 7200 3500
Connection ~ 7300 3500
Text Label 2700 3500 0    60   ~ 0
HVOSC
Wire Wire Line
	5450 4900 5450 4250
Wire Wire Line
	5450 4250 5550 4250
Wire Wire Line
	5450 3200 5450 3850
Wire Wire Line
	5450 3850 5550 3850
Text Notes 2500 2550 0    260  ~ 0
HVmux + HV filter
Text HLabel 2700 3500 0    60   Input ~ 0
HVOSC
Text HLabel 2700 4900 0    60   Input ~ 0
GND
Connection ~ 3450 4900
Text HLabel 6900 4500 3    60   Input ~ 0
HV_IN
Text HLabel 7200 4500 3    60   Input ~ 0
HV_IN_RTN
Text HLabel 8450 3500 2    60   Input ~ 0
HV_OUT_RTN
Text HLabel 8450 2700 2    60   Input ~ 0
HV_OUT
Connection ~ 8250 3500
Connection ~ 4550 4700
Connection ~ 4250 4700
Wire Wire Line
	6350 3850 6350 3900
Wire Wire Line
	6350 4200 6350 4250
$EndSCHEMATC
