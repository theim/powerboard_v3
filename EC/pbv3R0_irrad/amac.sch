EESchema Schematic File Version 2
LIBS:pbv3
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:pbv3-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Powerboard v3"
Date "2017-10-23"
Rev "A"
Comp "LBNL"
Comment1 "Timon Heim"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L AMACv2 U3
U 1 1 59F0B0F9
P 4400 3850
AR Path="/59F0B0F9" Ref="U3"  Part="1" 
AR Path="/59F0798F/59F0B0F9" Ref="U3"  Part="1" 
F 0 "U3" H 4400 4550 60  0000 C CNN
F 1 "AMACv2" H 4400 3550 160 0000 C CNN
F 2 "pbv3:AMACv2" H 3650 4500 60  0001 C CNN
F 3 "" H 3650 4500 60  0001 C CNN
	1    4400 3850
	1    0    0    -1  
$EndComp
Text HLabel 1450 900  0    60   Input ~ 0
VDCDC
Text HLabel 1450 600  0    60   Input ~ 0
VLREG
Text HLabel 1450 1300 0    60   Input ~ 0
GND
$Comp
L GND #PWR020
U 1 1 59F0BEEC
P 2050 1400
F 0 "#PWR020" H 2050 1150 50  0001 C CNN
F 1 "GND" H 2050 1250 50  0000 C CNN
F 2 "" H 2050 1400 50  0000 C CNN
F 3 "" H 2050 1400 50  0000 C CNN
	1    2050 1400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 59F0C190
P 1950 3150
F 0 "#PWR021" H 1950 2900 50  0001 C CNN
F 1 "GND" H 1950 3000 50  0000 C CNN
F 2 "" H 1950 3150 50  0000 C CNN
F 3 "" H 1950 3150 50  0000 C CNN
	1    1950 3150
	1    0    0    -1  
$EndComp
Text HLabel 4250 1100 1    60   Input ~ 0
CUR1Vp
Text HLabel 4450 1100 1    60   Input ~ 0
CUR1Vn
Text HLabel 4650 1100 1    60   Input ~ 0
DCDCEN
Text HLabel 4850 1100 1    60   Input ~ 0
NTCPBn
Text HLabel 5050 1100 1    60   Input ~ 0
NTCPBp
Text HLabel 5250 800  1    60   Input ~ 0
CUR10Vn
Text HLabel 5450 700  1    60   Input ~ 0
CUR10Vp
$Comp
L R R9
U 1 1 59F0D491
P 7200 2000
F 0 "R9" V 7280 2000 50  0000 C CNN
F 1 "14k" V 7200 2000 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 7130 2000 50  0001 C CNN
F 3 "" H 7200 2000 50  0000 C CNN
	1    7200 2000
	1    0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 59F0D4CA
P 7200 2400
F 0 "R10" V 7280 2400 50  0000 C CNN
F 1 "1k" V 7200 2400 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 7130 2400 50  0001 C CNN
F 3 "" H 7200 2400 50  0000 C CNN
	1    7200 2400
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 59F0D821
P 7450 2600
F 0 "R11" V 7530 2600 50  0000 C CNN
F 1 "1k" V 7450 2600 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 7380 2600 50  0001 C CNN
F 3 "" H 7450 2600 50  0000 C CNN
	1    7450 2600
	1    0    0    -1  
$EndComp
Text HLabel 7450 2350 1    60   Input ~ 0
PGOOD
Text HLabel 8450 3200 2    60   Input ~ 0
PTAT
Text HLabel 6450 3600 2    60   Input ~ 0
SSSHy
Text HLabel 6450 3800 2    60   Input ~ 0
HRSTBy
Text HLabel 6450 4000 2    60   Input ~ 0
LDy0EN
Text HLabel 6450 4200 2    60   Input ~ 0
HREFy
Text HLabel 6450 4400 2    60   Input ~ 0
NTCyN
Text HLabel 6450 4600 2    60   Input ~ 0
NTCyP
Text HLabel 6450 4800 2    60   Input ~ 0
LDy1EN
Text HLabel 6450 5000 2    60   Input ~ 0
LDy2EN
Text HLabel 6450 5200 2    60   Input ~ 0
SHUNTy
Text HLabel 6450 5400 2    60   Input ~ 0
CALy
Text HLabel 6450 5600 2    60   Input ~ 0
OFout
Wire Wire Line
	1450 1300 6300 1300
Wire Wire Line
	3150 1300 3150 1600
Wire Wire Line
	3350 1300 3350 1600
Connection ~ 3150 1300
Wire Wire Line
	3550 1300 3550 1600
Connection ~ 3350 1300
Wire Wire Line
	3750 1300 3750 1600
Connection ~ 3550 1300
Wire Wire Line
	3950 1300 3950 1600
Connection ~ 3750 1300
Wire Wire Line
	4150 1300 4150 1600
Connection ~ 3950 1300
Wire Wire Line
	4350 1300 4350 1600
Connection ~ 4150 1300
Wire Wire Line
	4550 1300 4550 1600
Connection ~ 4350 1300
Wire Wire Line
	4750 1300 4750 1600
Connection ~ 4550 1300
Wire Wire Line
	4950 1300 4950 1600
Connection ~ 4750 1300
Wire Wire Line
	5150 1300 5150 1600
Connection ~ 4950 1300
Wire Wire Line
	5350 1300 5350 1600
Connection ~ 5150 1300
Wire Wire Line
	5550 1300 5550 1600
Connection ~ 5350 1300
Wire Wire Line
	6300 1300 6300 6500
Wire Wire Line
	6300 2100 6050 2100
Connection ~ 5550 1300
Wire Wire Line
	6300 2300 6050 2300
Connection ~ 6300 2100
Wire Wire Line
	6050 2600 7200 2600
Connection ~ 6300 2300
Wire Wire Line
	6300 2900 6050 2900
Connection ~ 6300 2600
Wire Wire Line
	6300 3100 6050 3100
Connection ~ 6300 2900
Connection ~ 6300 3100
Wire Wire Line
	6050 3500 7700 3500
Connection ~ 6300 3300
Wire Wire Line
	6300 3700 6050 3700
Connection ~ 6300 3500
Wire Wire Line
	6300 3900 6050 3900
Connection ~ 6300 3700
Wire Wire Line
	6300 4100 6050 4100
Connection ~ 6300 3900
Wire Wire Line
	6300 4300 6050 4300
Connection ~ 6300 4100
Wire Wire Line
	6300 4500 6050 4500
Connection ~ 6300 4300
Wire Wire Line
	6300 4700 6050 4700
Connection ~ 6300 4500
Wire Wire Line
	6300 4900 6050 4900
Connection ~ 6300 4700
Wire Wire Line
	6300 5100 6050 5100
Connection ~ 6300 4900
Wire Wire Line
	6300 5300 6050 5300
Connection ~ 6300 5100
Wire Wire Line
	6300 5500 6050 5500
Connection ~ 6300 5300
Wire Wire Line
	6300 5700 6050 5700
Connection ~ 6300 5500
Wire Wire Line
	6300 6500 2550 6500
Connection ~ 6300 5700
Wire Wire Line
	4800 6500 4800 6300
Wire Wire Line
	4600 6500 4600 6300
Connection ~ 4800 6500
Wire Wire Line
	4400 6500 4400 6300
Connection ~ 4600 6500
Wire Wire Line
	4200 6500 4200 6300
Connection ~ 4400 6500
Wire Wire Line
	3900 6500 3900 6300
Connection ~ 4200 6500
Connection ~ 3900 6500
Wire Wire Line
	2550 5700 2850 5700
Wire Wire Line
	2550 5500 2850 5500
Connection ~ 2550 5700
Wire Wire Line
	2550 5300 2850 5300
Connection ~ 2550 5500
Wire Wire Line
	2550 5100 2850 5100
Connection ~ 2550 5300
Wire Wire Line
	2550 4900 2850 4900
Connection ~ 2550 5100
Wire Wire Line
	2550 4700 2850 4700
Connection ~ 2550 4900
Wire Wire Line
	2550 4500 2850 4500
Connection ~ 2550 4700
Wire Wire Line
	2550 4300 2850 4300
Connection ~ 2550 4500
Wire Wire Line
	2550 4100 2850 4100
Connection ~ 2550 4300
Wire Wire Line
	2550 3900 2850 3900
Connection ~ 2550 4100
Wire Wire Line
	2550 3700 2850 3700
Connection ~ 2550 3900
Wire Wire Line
	2550 3500 2850 3500
Connection ~ 2550 3700
Wire Wire Line
	2550 3300 2850 3300
Connection ~ 2550 3500
Wire Wire Line
	2550 3100 2850 3100
Connection ~ 2550 3300
Wire Wire Line
	2550 2400 2850 2400
Connection ~ 2550 3100
Connection ~ 2550 2400
Wire Wire Line
	2550 2200 2850 2200
Wire Wire Line
	2550 2100 2850 2100
Connection ~ 2550 2200
Wire Wire Line
	2050 1400 2050 1300
Connection ~ 2050 1300
Wire Wire Line
	1450 900  3650 900 
Wire Wire Line
	3450 900  3450 1600
Wire Wire Line
	3650 900  3650 1600
Connection ~ 3450 900 
Wire Wire Line
	1450 600  4050 600 
Wire Wire Line
	3850 600  3850 1600
Wire Wire Line
	4050 600  4050 1600
Connection ~ 3850 600 
Wire Wire Line
	2850 2600 1950 2600
Wire Wire Line
	1950 2600 1950 3150
Wire Wire Line
	2850 2700 1950 2700
Connection ~ 1950 2700
Wire Wire Line
	1950 2800 2850 2800
Connection ~ 1950 2800
Wire Wire Line
	2850 2900 1950 2900
Connection ~ 1950 2900
Wire Wire Line
	2850 3000 1950 3000
Connection ~ 1950 3000
Wire Wire Line
	4250 1100 4250 1600
Wire Wire Line
	4450 1100 4450 1600
Wire Wire Line
	4650 1100 4650 1600
Wire Wire Line
	4850 1100 4850 1600
Wire Wire Line
	5050 1100 5050 1600
Wire Wire Line
	5250 1150 5250 1600
Wire Wire Line
	5450 1150 5450 1600
Wire Wire Line
	6050 2200 7200 2200
Wire Wire Line
	7200 2150 7200 2250
Connection ~ 7200 2200
Wire Wire Line
	7200 800  7200 1850
Wire Wire Line
	7200 2600 7200 2550
Wire Wire Line
	7450 2450 7450 2350
Wire Wire Line
	6050 2800 7450 2800
Wire Wire Line
	6050 3200 8000 3200
Text HLabel 6700 2400 2    60   Input ~ 0
GPO2
Wire Wire Line
	6050 2400 6350 2400
NoConn ~ 6050 2500
NoConn ~ 6050 2700
NoConn ~ 6050 3000
Wire Wire Line
	6050 3600 6450 3600
Wire Wire Line
	6050 3800 6450 3800
Wire Wire Line
	6050 4000 6450 4000
Wire Wire Line
	6050 4200 6450 4200
Wire Wire Line
	6050 4400 6450 4400
Wire Wire Line
	6050 4800 6450 4800
Wire Wire Line
	6050 5000 6450 5000
Wire Wire Line
	6050 5200 6450 5200
Wire Wire Line
	6050 5400 6450 5400
Wire Wire Line
	6050 5600 6450 5600
Text HLabel 3600 6600 0    60   Input ~ 0
HVret1
Text HLabel 1700 7000 0    60   Input ~ 0
CMDin_P
Text HLabel 1700 6700 0    60   Input ~ 0
CMDin_N
Text HLabel 1700 7250 0    60   Input ~ 0
CMDout_P
Text HLabel 1700 7550 0    60   Input ~ 0
CMDout_N
Text HLabel 4300 6650 3    60   Input ~ 0
HVOSC0
Text HLabel 5100 6650 3    60   Input ~ 0
VDDHI
Text HLabel 5200 6650 3    60   Input ~ 0
LAM
Text HLabel 5400 7050 3    60   Input ~ 0
HVret2
Wire Wire Line
	3600 6300 3600 6600
Wire Wire Line
	3700 6700 3700 6300
Wire Wire Line
	3800 6300 3800 6750
Wire Wire Line
	4000 7250 4000 6300
Wire Wire Line
	4100 7550 4100 6300
Wire Wire Line
	4300 6300 4300 6650
NoConn ~ 4500 6300
NoConn ~ 4700 6300
NoConn ~ 4900 6300
$Comp
L C C17
U 1 1 59F0F7AD
P 5650 6750
F 0 "C17" H 5675 6850 50  0000 L CNN
F 1 "10nF" H 5675 6650 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 5688 6600 50  0001 C CNN
F 3 "" H 5650 6750 50  0000 C CNN
	1    5650 6750
	1    0    0    -1  
$EndComp
NoConn ~ 2850 2500
NoConn ~ 2850 3200
NoConn ~ 2850 3400
Text HLabel 2350 3600 0    60   Input ~ 0
LDx0EN
Text HLabel 2350 3800 0    60   Input ~ 0
HRSTBx
Text HLabel 2350 4000 0    60   Input ~ 0
SSSHx
Text HLabel 2350 4200 0    60   Input ~ 0
HREFx
Text HLabel 2350 4400 0    60   Input ~ 0
NTCxP
Text HLabel 2350 4600 0    60   Input ~ 0
NTCxN
Text HLabel 2350 4800 0    60   Input ~ 0
LDx1EN
Text HLabel 2350 5000 0    60   Input ~ 0
LDx2EN
Text HLabel 2350 5200 0    60   Input ~ 0
SHUNTx
Text HLabel 2350 5400 0    60   Input ~ 0
CALx
Text HLabel 2350 5600 0    60   Input ~ 0
OFin
Text HLabel 2350 5800 0    60   Input ~ 0
CAL
Wire Wire Line
	2850 3600 2350 3600
Wire Wire Line
	2850 3800 2350 3800
Wire Wire Line
	2850 4000 2350 4000
Wire Wire Line
	2850 4200 2350 4200
Wire Wire Line
	2850 4400 2350 4400
Wire Wire Line
	2850 4600 2350 4600
Wire Wire Line
	2850 4800 2350 4800
Wire Wire Line
	2850 5000 2350 5000
Wire Wire Line
	2850 5200 2350 5200
Wire Wire Line
	2850 5400 2350 5400
Wire Wire Line
	2850 5600 2350 5600
Wire Wire Line
	2850 5800 2350 5800
$Comp
L R R6
U 1 1 59F10673
P 5250 1000
F 0 "R6" V 5330 1000 50  0000 C CNN
F 1 "70k" V 5250 1000 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 5180 1000 50  0001 C CNN
F 3 "" H 5250 1000 50  0000 C CNN
	1    5250 1000
	1    0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 59F106AA
P 5450 1000
F 0 "R7" V 5530 1000 50  0000 C CNN
F 1 "70k" V 5450 1000 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 5380 1000 50  0001 C CNN
F 3 "" H 5450 1000 50  0000 C CNN
	1    5450 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 850  5250 800 
Wire Wire Line
	5450 700  5450 850 
$Comp
L C C13
U 1 1 59F4CE9B
P 2750 6700
F 0 "C13" H 2775 6800 50  0000 L CNN
F 1 "150pF" H 2775 6600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2788 6550 50  0001 C CNN
F 3 "" H 2750 6700 50  0000 C CNN
	1    2750 6700
	0    1    1    0   
$EndComp
$Comp
L C C14
U 1 1 59F4CEE4
P 2750 7000
F 0 "C14" H 2775 7100 50  0000 L CNN
F 1 "150pF" H 2775 6900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2788 6850 50  0001 C CNN
F 3 "" H 2750 7000 50  0000 C CNN
	1    2750 7000
	0    1    1    0   
$EndComp
$Comp
L C C15
U 1 1 59F4CF23
P 2750 7250
F 0 "C15" H 2775 7350 50  0000 L CNN
F 1 "150pF" H 2775 7150 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2788 7100 50  0001 C CNN
F 3 "" H 2750 7250 50  0000 C CNN
	1    2750 7250
	0    1    1    0   
$EndComp
$Comp
L C C16
U 1 1 59F4CF5F
P 2750 7550
F 0 "C16" H 2775 7650 50  0000 L CNN
F 1 "150pF" H 2775 7450 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 2788 7400 50  0001 C CNN
F 3 "" H 2750 7550 50  0000 C CNN
	1    2750 7550
	0    1    1    0   
$EndComp
Wire Wire Line
	1700 6700 2600 6700
Wire Wire Line
	1700 7000 2600 7000
Wire Wire Line
	1700 7250 2600 7250
Wire Wire Line
	1700 7550 2600 7550
Wire Wire Line
	2900 7250 4000 7250
Wire Wire Line
	2900 7550 4100 7550
Wire Wire Line
	2900 6700 3700 6700
Wire Wire Line
	2900 7000 3300 7000
Wire Wire Line
	3300 7000 3300 6750
Wire Wire Line
	3300 6750 3800 6750
Text Notes 4000 3750 0    60   ~ 0
3.5mm x 5.2mm
NoConn ~ 6050 5800
Wire Wire Line
	6050 4600 6450 4600
$Comp
L C C18
U 1 1 59F573B1
P 7700 3350
F 0 "C18" H 7725 3450 50  0000 L CNN
F 1 "1nF" H 7725 3250 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 7738 3200 50  0001 C CNN
F 3 "" H 7700 3350 50  0000 C CNN
	1    7700 3350
	1    0    0    -1  
$EndComp
Connection ~ 7700 3200
$Comp
L R R13
U 1 1 59F57760
P 8150 3200
F 0 "R13" V 8230 3200 50  0000 C CNN
F 1 "10k" V 8150 3200 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 8080 3200 50  0001 C CNN
F 3 "" H 8150 3200 50  0000 C CNN
	1    8150 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	8300 3200 8450 3200
$Comp
L R R8
U 1 1 59F579B7
P 6500 2400
F 0 "R8" V 6580 2400 50  0000 C CNN
F 1 "1k" V 6500 2400 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 6430 2400 50  0001 C CNN
F 3 "" H 6500 2400 50  0000 C CNN
	1    6500 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 2400 6700 2400
Text Notes 7700 2800 0    60   ~ 0
GPO will be used for DCDCadj
$Comp
L R R5
U 1 1 59F7449B
P 6500 3400
F 0 "R5" V 6580 3400 50  0000 C CNN
F 1 "1k" V 6500 3400 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 6430 3400 50  0001 C CNN
F 3 "" H 6500 3400 50  0000 C CNN
	1    6500 3400
	0    1    1    0   
$EndComp
Text HLabel 6700 3400 2    60   Input ~ 0
GPO1
Wire Wire Line
	6050 3400 6350 3400
Wire Wire Line
	6650 3400 6700 3400
Text Notes 1850 800  0    60   ~ 0
Do we need bypassing for these?\n
Wire Wire Line
	5000 6300 5000 6500
Connection ~ 5000 6500
Wire Wire Line
	5100 6300 5100 6650
Wire Wire Line
	5200 6300 5200 6650
Wire Wire Line
	5300 6300 5300 6500
Connection ~ 5300 6500
Wire Wire Line
	5400 6300 5400 7050
Text Notes 4350 7300 0    60   ~ 0
No LAM on bus tape.
$Comp
L C C12
U 1 1 59F6B083
P 3200 6300
F 0 "C12" H 3225 6400 50  0000 L CNN
F 1 "10nF" H 3225 6200 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 3238 6150 50  0001 C CNN
F 3 "" H 3200 6300 50  0000 C CNN
	1    3200 6300
	0    1    1    0   
$EndComp
Wire Wire Line
	3500 6300 3350 6300
Wire Wire Line
	3050 6300 2900 6300
Wire Wire Line
	2900 6300 2900 6500
Connection ~ 2900 6500
Text Notes 900  2850 0    60   ~ 0
ID to be selected \nby pulling wirebonds.
Wire Wire Line
	5500 6300 5500 6400
Wire Wire Line
	5500 6400 5650 6400
Wire Wire Line
	5650 6400 5650 6600
Wire Wire Line
	5650 6900 5650 6950
Wire Wire Line
	5650 6950 5400 6950
Connection ~ 5400 6950
Text Label 2950 6700 0    60   ~ 0
CMDin*_N
Text Label 2950 7000 0    60   ~ 0
CMDin*_P
Text Label 2950 7250 0    60   ~ 0
CMDout*_P
Text Label 2950 7550 0    60   ~ 0
CMDout*_N
Wire Wire Line
	7200 800  5450 800 
Connection ~ 5450 800 
Wire Wire Line
	6050 3300 6300 3300
Wire Wire Line
	7450 2800 7450 2750
Wire Wire Line
	2550 6500 2550 2100
NoConn ~ 2850 2300
$Comp
L R R403
U 1 1 5AE72316
P 3000 6850
F 0 "R403" V 3080 6850 50  0000 C CNN
F 1 "20k" V 3000 6850 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 2930 6850 50  0001 C CNN
F 3 "" H 3000 6850 50  0000 C CNN
	1    3000 6850
	1    0    0    -1  
$EndComp
$Comp
L R R401
U 1 1 5AE7293D
P 2300 6850
F 0 "R401" V 2380 6850 50  0000 C CNN
F 1 "100R" V 2300 6850 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 2230 6850 50  0001 C CNN
F 3 "" H 2300 6850 50  0000 C CNN
	1    2300 6850
	1    0    0    -1  
$EndComp
$Comp
L R R402
U 1 1 5AE729B3
P 2300 7400
F 0 "R402" V 2380 7400 50  0000 C CNN
F 1 "100R" V 2300 7400 50  0000 C CNN
F 2 "Resistors_SMD:R_0201" V 2230 7400 50  0001 C CNN
F 3 "" H 2300 7400 50  0000 C CNN
	1    2300 7400
	1    0    0    -1  
$EndComp
Connection ~ 2300 6700
Connection ~ 2300 7000
Connection ~ 2300 7250
Connection ~ 2300 7550
Connection ~ 3000 7000
Connection ~ 3000 6700
$EndSCHEMATC
